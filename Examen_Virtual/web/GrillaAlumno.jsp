<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="myApp">
    <head>
        <title>Consulta Alumno</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="css/base.css" rel = "stylesheet" type="text/css"/>
       
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
       
        <script src="jquery/query.js" type="text/javascript"></script>
        <script src="js/validar.js" type="text/javascript"></script>
        
        <!--Para AngularJS-->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
        <script src="js/myapp.js"> </script>
        <script src="js/mycontroller.js"></script>
        
    </head>
    <body ng-controller="MyController">
        <div  class="header"  style="color: blanchedalmond; font-family: fantasy; font-size: 30px; text-align: center;">
            Consulta Alumno
        </div>

        <div class="content" >
            <form class="form-inline" action="" id="frmFiltro" name="frmFiltro">
                Filtro:
                <div class="form-group">
                    <select name="sltFiltro" id="sltFiltro" ng-model="select" ng-change="Activar2()">
                        <option value="0" selected >Seleccionar</option>
                        <option value="1">Codigo</option>
                        <option value="2">Nota</option>
                    </select>
                </div> 
                <div class="form-group">
                    <input type="text" id="inFiltro" name="inFiltro"  size="12" ng-keyup="getDataFromServer() " ng-model="value" disabled >
                </div>                
            </form>
            
            <div style="width: 600px;" >
                <table class="table table-bordered">
                    <tr>
                        <th>Id</th>
                        <th>Codigo</th>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Nota</th>
                        <th>VALUE</th>
                    </tr>
                   
                    <tr ng-repeat="alumno in alumnos">
                        <td>{{alumno.id}}</td>
                        <td>{{alumno.codigo}}</td>
                        <td>{{alumno.nombre}}</td>
                        <td>{{alumno.apellido}}</td>
                        <td>{{alumno.nota}}</td>
                        <td>{{value}}</td>
                    </tr>

                </table>
            </div > 

        </div>

        <div class="bottom">
            &COPY; 2016
        </div>

    </body>
</html>