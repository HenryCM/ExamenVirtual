
            function activar() {
                if (document.getElementById("sltFiltro").options[document.getElementById("sltFiltro").selectedIndex].value != "0")
                {
                                    document.getElementById("inFiltro").value = "  ";
                
                    document.getElementById("inFiltro").disabled = false;
                    document.getElementById("Enviar").disabled = false;
                } else
                {
                    document.getElementById("inFiltro").disabled = true;
                    document.getElementById("Enviar").disabled = true;
                    document.getElementById("inFiltro").value = "";
                }
                    
                    
                  document.getElementById("inFiltro").value = "  ";

            }

            function ExisteValor(valor) {
                var todoCorrecto = true;
                if (valor == null || valor.length == 0 || /^\s*$/.test(valor)) {
                    alert('ERROR: El filtro no puede estar vacío o contener sólo espacios en blanco');
                    todoCorrecto = false;
                }
                return todoCorrecto;
            }

            function EsNumero(valor) {
                var todoCorrecto = true;
                for (i = 0; i < valor.length; i++) {
                    if (isNaN(valor[i])) {
                        if (valor[i] != ".") {
                            todoCorrecto = false;
                            alert("ERROR, No numerico");
                            break;
                        }
                    }
                }
                return todoCorrecto;
            }

            function Rango(valor) {
                var todoCorrecto = true;
                if (valor > 20 || valor < 0) {
                    alert('ERROR: El valor no esta en el rango de [0,20] ');
                    todoCorrecto = false;
                }
                return todoCorrecto;
            }

            function Tamano(valor) {
                var todoCorrecto = true;
                if (valor.length > 8) {
                    alert('ERROR:exceso de digitos');
                    todoCorrecto = false;
                }
                return todoCorrecto;
            }

            function Positivo(valor) {
                var todoCorrecto = true;
                if (valor < 0) {
                    alert('ERROR: valor negativo');
                    todoCorrecto = false;
                }
                return todoCorrecto;
            }

            function validar() {
                document.getElementById("inFiltro").disabled = true;
                document.getElementById("Enviar").disabled = true;
                var i;
                var todoCorrecto = true;
                var nFiltro = document.getElementById("sltFiltro").options[document.getElementById("sltFiltro").selectedIndex].value;
                var Filtro = document.getElementById("inFiltro").value;

                if (nFiltro == "1") {

                    if (!ExisteValor(Filtro) || !Positivo(Filtro) || !Tamano(Filtro) || !EsNumero(Filtro)) {
                        todoCorrecto = false;
                    }

                } else {
                    if (nFiltro == "2") {
                        if (!ExisteValor(Filtro) || !Rango(Filtro) || !EsNumero(Filtro)) {
                            todoCorrecto = false;
                        }
                    }
                }
                if (todoCorrecto == true) {
                    location.href = "alumno?op=listaFiltro&slt=" + nFiltro + "&value=" + Filtro;
                    alert("Lista Actualizada");
                }

                document.getElementById("inFiltro").disabled = true;
                document.getElementById("Enviar").disabled = true;
                document.getElementById("inFiltro").value = "";
                document.getElementById("sltFiltro").selectedIndex = "0";
            }


