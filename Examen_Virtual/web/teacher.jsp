<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="myApp">
    <head>
        <title>Examen Virtual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="css/base.css" rel = "stylesheet" type="text/css"/>
       
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
       
        <script src="jquery/query.js" type="text/javascript"></script>
        <script src="js/validar.js" type="text/javascript"></script>
        
        <!--Para AngularJS-->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
        <script src="js/myapp.js"> </script>
        <script src="js/mycontroller.js"></script>
        
    </head>
    <body ng-controller="MyController">
        <div  class="header"  style="color: blanchedalmond; font-family: fantasy; font-size: 30px; text-align: center;">
            EXAMEN VIRTUAL
        </div>
        
        <div class="content" style=" alignment-baseline: central; " >
            <h1 style=" font-family: fantasy; font-size: 25px; text-align: center;"> Iniciar Sesion</h1>
            <form class="form-inline" action="" id="frmLogin" name="frmLogin">
              
                <table  style="width: 300px; border-collapse: separate; border-spacing: 15px 20px;">
                    <tr >
                        <th>User: </th>
                        <th><input type="text" id="inUser" name="inUser"  size="12" ng-model="valueUser" >  </th>
                        
                    </tr>    
                
                    <tr >      
                        <th>Password:  </th>
                        <th><input type="text" id="inPass" name="inPass"  size="12" ng-model="valuePass" ></th>
                  
                    </tr>
                  
                    <tr >
                        <th>  </th>
                        <th><input class="btn btn-mini btn-success" type="button" value="Login" id="btnLogin" name="btnLogin" ng-click="getDataFromServer()" ></th>
                    
                    </tr>
                    
                    
                    
                </table>
              
            </form>

        </div>
        
        

        <div class="bottom">
            &COPY; 2016
        </div>

    </body>
</html>