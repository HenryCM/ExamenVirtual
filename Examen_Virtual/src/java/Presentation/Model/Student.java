package Presentation.Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author CuentaLocal
 */
public class Student {
    private String Code;
    private String Year_study_plan;
    private String Id_school;
    private String Id_faculty;
    private String First_name;
    private String Last_name;

    public Student(String Code, String Year_study_plan, String Id_school, String Id_faculty, String First_name, String Last_name) {
        this.Code = Code;
        this.Year_study_plan = Year_study_plan;
        this.Id_school = Id_school;
        this.Id_faculty = Id_faculty;
        this.First_name = First_name;
        this.Last_name = Last_name;
    }

    public String getCode() {
        return Code;
    }

    public String getYear_study_plan() {
        return Year_study_plan;
    }

    public String getId_school() {
        return Id_school;
    }

    public String getId_faculty() {
        return Id_faculty;
    }

    public String getFirst_name() {
        return First_name;
    }

    public String getLast_name() {
        return Last_name;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public void setYear_study_plan(String Year_study_plan) {
        this.Year_study_plan = Year_study_plan;
    }

    public void setId_school(String Id_school) {
        this.Id_school = Id_school;
    }

    public void setId_faculty(String Id_faculty) {
        this.Id_faculty = Id_faculty;
    }

    public void setFirst_name(String First_name) {
        this.First_name = First_name;
    }

    public void setLast_name(String Last_name) {
        this.Last_name = Last_name;
    }
    
    
    
}
