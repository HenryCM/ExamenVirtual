/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation.Model;

/**
 *
 * @author Jhosep
 */
public class User_system {
    private String id_user_system;
    private String code_teacher;
    private String code_student;
    private String user_user_system;
    private String password_user_system;
    private String type_user_system;

    public User_system(String id_user_system, String code_teacher, String code_student, String user_user_system, String password_user_system, String type_user_system) {
        this.id_user_system = id_user_system;
        this.code_teacher = code_teacher;
        this.code_student = code_student;
        this.user_user_system = user_user_system;
        this.password_user_system = password_user_system;
        this.type_user_system = type_user_system;
    }

    public String getId_user_system() {
        return id_user_system;
    }

    public void setId_user_system(String id_user_system) {
        this.id_user_system = id_user_system;
    }

    public String getCode_teacher() {
        return code_teacher;
    }

    public void setCode_teacher(String code_teacher) {
        this.code_teacher = code_teacher;
    }

    public String getCode_student() {
        return code_student;
    }

    public void setCode_student(String code_student) {
        this.code_student = code_student;
    }

    public String getUser_user_system() {
        return user_user_system;
    }

    public void setUser_user_system(String user_user_system) {
        this.user_user_system = user_user_system;
    }

    public String getPassword_user_system() {
        return password_user_system;
    }

    public void setPassword_user_system(String password_user_system) {
        this.password_user_system = password_user_system;
    }

    public String getType_user_system() {
        return type_user_system;
    }

    public void setType_user_system(String type_user_system) {
        this.type_user_system = type_user_system;
    }




    
}
