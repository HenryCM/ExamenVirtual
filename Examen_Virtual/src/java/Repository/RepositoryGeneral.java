package Repository;

import Presentation.Model.Student;
import Presentation.Model.User_system;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author CuentaLocal
 */
public class RepositoryGeneral {
    Connection conn;

    public RepositoryGeneral() {
        this.conn = ConnectionOracle.Conector();
        if (this.conn == null) {
            System.out.println("no hay conexion");
            System.exit(1);
        }
    }


    public ArrayList<Student> listStudent() throws SQLException {
        ArrayList<Student> list=new ArrayList<>();
        Statement preparedStatement = null;
        ResultSet resultSet = null;
        String query="select * from student" ;
        
        try {
            preparedStatement = this.conn.createStatement();
            resultSet = preparedStatement.executeQuery(query);
            return getListStudent(resultSet); 
        } catch (SQLException e) {
        } finally {
            preparedStatement.close();
            resultSet.close();
        }
        return null;
    }
    
    
    public  ArrayList<User_system> searchUserSystem(String user_user, String password_user) throws SQLException {
        Statement preparedStatement = null;
        ResultSet resultSet = null;
        if(user_user!= null ){
            System.out.println(user_user);
        }else
            System.out.println("no hay datos");
        String query="select * from user_system Where user_user_system = '" + user_user + "' AND password_user_system = '" + password_user +"'" ;
        
        try {
            preparedStatement = this.conn.createStatement();
            resultSet = preparedStatement.executeQuery(query);
            return getUserSystem(resultSet);
        } catch (SQLException e) {
        } finally {
            preparedStatement.close();
            resultSet.close();
        }
        return null;
    }
    
    /*
        public ArrayList<Student> listTeacher() throws SQLException{
        ArrayList<Student> list=new ArrayList<>();
        Statement preparedStatement = null;
        ResultSet resultSet = null;
        String query="select * from teacher" ;
        
        try {
            preparedStatement = this.conn.createStatement();
            resultSet = preparedStatement.executeQuery(query);
            return getListTeacher(resultSet); 
        } catch (SQLException e) {
        } finally {
            preparedStatement.close();
            resultSet.close();
        }
        return null;
    }*/
        
        
    private ArrayList<Student>/*ArrayList<Student>*/ getListStudent(ResultSet resultSet) throws SQLException {
        ArrayList<Student> listStudent = new ArrayList<>();
        //ArrayList<String> Re=new ArrayList<>();
        Student student;
        //String resultado="";
        while (resultSet.next()) {
            student = new Student(resultSet.getString("code_student"),resultSet.getString("year_study_plan"), resultSet.getString("id_school"),resultSet.getString("id_faculty"),resultSet.getString("firstname_student"),resultSet.getString("lastname_student"));
            listStudent.add(student);
            /*resultado+=resultSet.getString("code_student")+resultSet.getString("year_study_plan")+ resultSet.getString("id_school")+
                    resultSet.getString("id_faculty")+resultSet.getString("firstname_student")+resultSet.getString("lastname_student")+"\n";
            Re.add(resultado);*/
        }
        return listStudent;
    }

   private ArrayList<User_system> getUserSystem(ResultSet resultSet) throws SQLException {
        ArrayList<User_system> listUser = new ArrayList<>();
        //ArrayList<String> Re=new ArrayList<>();
       User_system user;
        //String resultado="";
        while (resultSet.next()) {
            user = new User_system(resultSet.getString("id_user_system"),resultSet.getString("code_teacher"), resultSet.getString("code_student"), resultSet.getString("user_user_system"),resultSet.getString("password_user_system"),resultSet.getString("type_user_system") );
            listUser.add(user);
            /*resultado+=resultSet.getString("code_student")+resultSet.getString("year_study_plan")+ resultSet.getString("id_school")+
                    resultSet.getString("id_faculty")+resultSet.getString("firstname_student")+resultSet.getString("lastname_student")+"\n";
            Re.add(resultado);*/
        }
        return listUser;
    }
    /*
    private ArrayList<String>/*ArrayList<Student> getListTeacher(ResultSet resultSet) throws SQLException {
        //ArrayList<Student> listStudent = new ArrayList<>();
        ArrayList<String> Re=new ArrayList<>();
        //Student student;
        String resultado="";
        while (resultSet.next()) {
            //restudent = new Student(resultSet.getString("code_student"),resultSet.getString("year_study_plan"), resultSet.getString("id_school"),resultSet.getString("id_faculty"),resultSet.getString("firstname_student"),resultSet.getString("lastname_student"));
            //listStudent.add(student);
            resultado+=resultSet.getString("code_student")+resultSet.getString("year_study_plan")+ resultSet.getString("id_school")+
                    resultSet.getString("id_faculty")+resultSet.getString("firstname_student")+resultSet.getString("lastname_student")+"\n";
            Re.add(resultado);
        }
        return Re;
    }*/
    
}
