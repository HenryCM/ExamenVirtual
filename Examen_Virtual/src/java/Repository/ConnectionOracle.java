package Repository;

import java.sql.Connection;
import java.sql.DriverManager;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author CuentaLocal
 */
public class ConnectionOracle {
    
    
    public static Connection Conector(){
        try {
            Class.forName("oracle.jdbc.OracleDriver");
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl","sys as sysdba","oracle12c");
            return conn;
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    
    }
    
}
